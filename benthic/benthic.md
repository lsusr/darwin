Welcome to the depths of the sea, far beyond the reach of sunlight. Here ye shall feast upon the dead.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/benthic/04_EX1304_dive13_NOAAOER.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Benthic | 10 | 0 | 0 | 0 | 1000 | 0 | 0 | 0 |

72 species were submitted to the Benthic.

# Generations 0-25

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/benthic/benthic-0-25.png)

We start with a population crash, as usual. After that, the populations almost look like they stabilize. Fewer than half of our species have gone extinct.

| Goes Extinct in Generation | Species |
|---|---|
| 6 | Salpophore |
| 6 | Tasty |
| 7 | Marine Snow Worm |
| 7 | benthic bottom feeder |
| 8 | Scavenger |
| 9 | Isopoid |
| 9 | Detritus Eater Eater |
| 10 | Scavenger Shrimp |
| 10 | Rubble on Double |
| 10 | Sea Zombie |
| 11 | Fangliphore |
| 11 | Grumpy Jellyfish |
| 11 | Doomed |
| 11 | Happy Hunter |
| 11 | A new hope |
| 12 | Krill1 |
| 12 | Sp0r3 |
| 12 | Cnidophore |
| 12 | Cephalophore |
| 12 | Not Picky |
| 13 | Detritus Eater Eater Eater |
| 14 | Sea Snail |
| 14 | Hermit Crab |
| 14 | Space Horse |
| 14 | BeauOmni3 |
| 14 | aBa-CG751 |
| 15 | Flounder |
| 16 | Microbet |
| 16 | Slowmo |
| 17 | aBa-D273 |
| 18 | Hund |
| 18 | Glyptoderm |
| 18 | Squid Kid |
| 19 | Anglerfish |
| 19 | Toxic Squid |
| 20 | Torpedo Jelly |
| 23 | Willi |
| 24 | cg-riverfish |

Here are our populations at generation 25.

| Population | Species |
|---|---|
| 170 | Barnacle |
| 158 | Deepwater Devourer |
| 124 | The Infinite Depths |
| 114 | FOERDI 1,6,4,3,D |
| 98 | Flutz |
| 88 | DetriusSpeedTank |
| 66 | Spanish Beard |
| 55 | BeauOmni1 |
| 50 | Silli |
| 46 | FOERDI 1,0,10,1,D |
| 45 | aBa-D262 |
| 43 | Tilli |
| 40 | FOERDI 0,0,0,10,D |
| 39 | FOERDI 1,3,7,3,D |
| 35 | Pilli |
| 34 | Blood Shark |
| 33 | FOERDI 1,10,0,1,D |
| 32 | Xilli |
| 30 | Chilli |
| 29 | omastar |
| 27 | Elephantfish |
| 27 | Abyssal eel |
| 24 | Billi |
| 19 | Kraken2 |
| 17 | Space Fiddler |
| 15 | Giant Squid |
| 10 | The Tideless Depths |
| 5 | FOERDI 1,9,1,1,D |
| 5 | Soonbegon |
| 2 | gonnabiteit |
| 2 | Deap Sea Tortoise |
| 2 | Flesh-Eating Clam1 |
| 1 | Killi |
| 1 | Tachyphore |

As you might guess, the Barnicle is an invincible Detritus eater and the Deepwater Devourer hunts. However, the Deepwater Devourer is not an apex predator. It has only 3 Attack and 6 speed. (It also has Antivenom and the ability to consume Detritus.)

# Generations 25-100

Barnicles dominate.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/benthic/benthic-25-100.png)

| Goes Extinct in Generation | Species |
|---|---|
| 6 | Salpophore |
| 6 | Tasty |
| 7 | Marine Snow Worm |
| 7 | benthic bottom feeder |
| 8 | Scavenger |
| 9 | Isopoid |
| 9 | Detritus Eater Eater |
| 10 | Scavenger Shrimp |
| 10 | Rubble on Double |
| 10 | Sea Zombie |
| 11 | Fangliphore |
| 11 | Grumpy Jellyfish |
| 11 | Doomed |
| 11 | Happy Hunter |
| 11 | A new hope |
| 12 | Krill1 |
| 12 | Sp0r3 |
| 12 | Cnidophore |
| 12 | Cephalophore |
| 12 | Not Picky |
| 13 | Detritus Eater Eater Eater |
| 14 | Sea Snail |
| 14 | Hermit Crab |
| 14 | Space Horse |
| 14 | BeauOmni3 |
| 14 | aBa-CG751 |
| 15 | Flounder |
| 16 | Microbet |
| 16 | Slowmo |
| 17 | aBa-D273 |
| 18 | Hund |
| 18 | Glyptoderm |
| 18 | Squid Kid |
| 19 | Anglerfish |
| 19 | Toxic Squid |
| 20 | Torpedo Jelly |
| 23 | Willi |
| 24 | cg-riverfish |
| 28 | gonnabiteit |
| 29 | Killi |
| 33 | Tachyphore |
| 35 | Giant Squid |
| 40 | Space Fiddler |
| 40 | Xilli |
| 40 | Pilli |
| 40 | FOERDI 1,9,1,1,D |
| 43 | Flesh-Eating Clam1 |
| 44 | Soonbegon |
| 47 | Deap Sea Tortoise |
| 63 | omastar |
| 63 | aBa-D262 |
| 65 | FOERDI 1,3,7,3,D |
| 66 | Kraken2 |
| 68 | BeauOmni1 |
| 73 | Elephantfish |
| 75 | Billi |
| 78 | Flutz |
| 82 | FOERDI 1,6,4,3,D |
| 83 | Blood Shark |
| 83 | Silli |
| 89 | The Tideless Depths |

# Generations 100-1000

Most of our species die out leaving only Barnicles, Spanish Beards and Tillis.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/benthic/benthic-100-1000.png)

You can't see it in the above graph but a few Soonbegons trickle in to compete for Detritus.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/benthic/benthic-soonbegons.png)

The Barnicle and Spanish Beard are identicle invincible detritivores. The Tilli occupies a slightly different niche by foraging for Carrion too.

| Goes Extinct in Generation | Species |
|---|---|
| 125 | FOERDI 1,10,0,1,D |
| 152 | DetriusSpeedTank |
| 170 | FOERDI 0,0,0,10,D |
| 183 | Abyssal eel |
| 203 | FOERDI 1,0,10,1,D |
| 230 | The Infinite Depths |
| 232 | Deepwater Devourer |
| 257 | Chilli |

# Winners

| Name | Venom? | Attack | Armor | Speed | Forage? | Creator | Social Media |
|---|---|---|---|---|---|---|---|
| Barnacle | Antivenom only | 0 | 10 | 0 | Detritus | Guy Srinivasan | | None |
| Spanish Beard | Antivenom only | 0 | 10 | 0 | Detritus | VJ | None |
| Tilli | Antivenom only | 0 | 10 | 0 | Carrion;Detritus | Milli | None |
| Soonbegon | Venom + Antivenom | 0 | 0 | 10 | Detritus | Martin Randall | [LinkedIn](https://www.linkedin.com/in/martin-harper-randall/) |
