| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Tundra | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 300 |

Our Tundra is an inhospitable[^1] environment. The only significant food available to herbivores is Lichen, which has a tiny nutritional value of 1. The Tundra is cold too. Staying warm requires the cold tolerance adaptation, which costs +2 size.

[^1]: The original Tundra was even more inhospitable than this. I made it easier thanks to early feedback from aphyer.

An organism must expend 20% of its energy just to survive. A herbivore foraging for lichen cannot have a size greater than 5 or else it will expend more energy in metabolism than it is possible to acquire from Lichen.

All organisms have base size 0.1. The cold adaptation (+2) plus the Lichen digestive tract (+1) costs a total of +3 size. A Tundra herbivore has a minimum size of 3.1. A herbivore with size 5.1 is untenable since it expends more energy than is possible to obtain from Lichen.

Players submitted 39 species native to the Tundra. Only 4 of them were viable herbivores: Micropas, Arctic Slug, Northern Nibbler and lichen (not to be confused with the foragable "Lichen"). (Multicore's arctic fox was a carnivore.)

These species could support little in the way of weapons, armor and speed. They were defenseless. In the first 8 turns, all four of our viable foragers are eaten to extinction.

| Goes Extinct in Generation | Species |
|---|---|
| 5 | Pristol |
| 7 | Micropas |
| 7 | Arctic Slug |
| 8 | Northern Nibbler |
| 8 | lichen |

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/tundra/tundra-0-8.png)

After the viable herbivores were eliminated, total ecological collapse is inevitable.

| Goes Extinct in Generation | Species |
|---|---|
| 9 | Yonge_Cold |
| 9 | Boreakeet |
| 9 | Beck’s Penguin |
| 10 | SmolFire |
| 10 | Arctic Ambusher |
| 10 | Zlorg |
| 10 | Arctic Fox |
| 10 | Orange-Krill |
| 10 | abominable_snowman |
| 12 | Antasvara |
| 12 | Unfortunately Large Cockroach |
| 12 | cg-mouse |
| 13 | Porostozer Malutki |
| 13 | 1994 Mazda RX7 |
| 14 | Raburetta |
| 14 | Pittsburgh-Penguins |
| 15 | Louse-lion |
| 15 | Wolverine |
| 16 | Jtp |
| 16 | Wolves |
| 17 | Seals |
| 19 | Direwolf |
| 24 | Tsc |
| 27 | Tundrus Rex |
| 29 | Frankenstein |
| 32 | Broken Fetters |
| 34 | Alaskans |
| 37 | Dragon |
| 37 | Porostozer Mamuci |
| 39 | Rocks |
| 41 | Duckofants |
| 43 | White-Whales |
| 50 | tp511 |
| 52 | Frostwing Snipper |

# The Frostwing Snipper

An honorable mention goes to Nem's Frostwing Snipper, a Speed 10 species that could digest both Lichen and Seeds. The maximum speed made the Frostwing Snipper immune to predation which let it survive the initial carnage. The ability to digest seeds meant that the Frostwing Snipper did consume enough energy on average to more than replace itself.

However, "on average" is not enough. Its average population was too small. Random fluctuations eventually knocked the Frostwing Snipper into extinction.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/tundra/frostwing-snipper.png)

A similar strategy with armor and antivenom instead of speed and/or carrion instead of seeds might have been viable, but nobody attempted those strategies.

# Winners

None
