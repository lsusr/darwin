#!/home/lsusr/.local/bin/hy
(import [pandas :as pd]
        [matplotlib.pyplot :as plt])
(setv df (pd.read-csv "csvs/entries.csv"))

(setv pie-measure "Forager?")

(setv vc (pd.Series))
(setv (. vc ["Hunters"]) 0)
(setv (. vc ["Foragers"]) 0)
(setv (. vc ["Omnivores"]) 0)

(setv fast-armor 0)
(for [[_ row] (df.iterrows)]
  #_(setv [timestamp species-name spawning-zone venom weapons armor
         speed foraging temperature human-name] row)
  (setv max-speed (= 10 (. row ["Speed"])))
  (setv armor (> 0 (. row ["Armor"])))
  (if (and max-speed armor)
    (+= fast-armor 1))

  #_(setv is-forager (not (pd.isnull (. row ["Forager?"]))))
  #_(setv is-predator (< 0 (. row ["Weapons"])))
  #_(print is-predator)
  #_(if (and is-forager is-predator)
    (+= (. vc ["Omnivores"]) 1)
    is-forager
    (+= (. vc ["Foragers"]) 1)
    is-predator
    (+= (. vc ["Hunters"]) 1)
    #_True
    #_(print "error"))
  ;(print (. row ["Weapons"]))
  #_(print row))
(print "count is " fast-armor)

;(setv vc (.value-counts (. df [pie-measure])))
;(setv (. vc ["Multiple"]) 0)
(for [index (. vc index)]
  (if (in ";" index)
    (do
   (+= (. vc ["Multiple"]) 1)
  (setv vc
  (vc.drop index
               )))))
;(print vc)
;(setv duplicate-index (in ";"
;(print (. vc index))
(setv [patches texts] (plt.pie vc))
;(print (.value-counts (. df [pie-measure])))
(plt.legend patches (. vc index) :loc "upper left")

(plt.show)

#_(do
(setv measure "Armor")
(plt.bar (range 11) (lfor i (range 11) (. (.value-counts (. df [measure])) [i])))
(plt.ylim 0 350)
(plt.title measure)
)
