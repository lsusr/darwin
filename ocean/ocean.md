The Ocean has lots of algae, a little carrion and a little detritus. It was the most popular biome, with 131 native species.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean.jpeg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Ocean | 10 | 0 | 0 | 0 | 10 | 0 | 10,000 | 0 |

To thrive in this game as a forager you must survive the initial predators. One way to do this is to breed faster than they can eat you. Another way is to use armor or another defense.

Random guy in NY took a third route. He created a First Round Predator Distractor, a cheap species with almost zero nutrition and exactly zero chance of survival that kept the predators in check for a single round.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-0-1.png)

| Goes Extinct in Generation | Species |
|---|---|
| 1 | First Round Predator Distractor |

Now that Random guy in NY's silliness is out of the way let's get on to the real show.

# Generations 1-50

At first the defenseless foragers thrive. Increasingly large carnivores's populations grow and crash.

| Name | Venom + Antivenom? | Attack | Armor | Speed | Forager? | Creator |
|---|---|---|---|---|---|---|---|
| Phytoplankton | Neither | 0 | 0 | 0 | Algae | aphyer |
| Algae Tribble | Neither | 0 | 0 | 0 | Algae | simon |
| CookieMaximizer | Neither | 1 | 0 | 1 |  | Saran |
| gkdemaree-9-eater-of-drifting-forager | Neither | 1 | 0 | 1 |  | Grant Demaree |
| Pike | Neither | 2 | 0 | 3 |  | Yair |
| Herring | Neither | 2 | 0 | 2 |  | Measure |
| Bhuphin | Neither | 0 | 2 | 0 | Algae | phenx |
| Snail-Eating Snail | Antivenom only | 5 | 0 | 2 | Carrion;Seeds;Detritus | Taleuntum |
| Jilli | Neither | 3 | 0 | 1 | Algae | Milli |
| Right whale | Neither | 5 | 0 | 5 |  | EricF |
| gkdemaree-10-small-shark | Neither | 6 | 0 | 6 |  | Grant Demaree |
| gkdemaree-1-fast-shark | Antivenom only | 10 | 0 | 10 |  | Grant Demaree |
| gkdemaree-6-large-omnivore-fish | Antivenom only | 7 | 0 | 7 | Algae | Grant Demaree |

[![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-1-50.png)](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-1-50.png)

| Goes Extinct in Generation | Species |
|---|---|
| 7 | Cheerless Tidehunters |
| 12 | Krabs |
| 12 | Qoxeadian |
| 13 | Bloade |
| 13 | Squidheads |
| 14 | Sleeping Sealions |
| 16 | Sanguine Scavenger |
| 17 | Terror Whale |
| 17 | Sea Snake |
| 17 | Whale |
| 17 | Algae Sprinter |
| 17 | FOERDI 1,0,10,0,CDA |
| 18 | Blue whale |
| 18 | Chybcik  |
| 18 | op511 |
| 18 | Crab |
| 19 | Kraken1 |
| 19 | The Aparatus |
| 19 | ocean vulture |
| 20 | magikarp |
| 20 | sea tortoise |
| 20 | 1-4-1 carrion |
| 21 | osc511 |
| 22 | Manta Ray |
| 22 | Grerft |
| 24 | Plankton2 |
| 24 | Poisonous Sponge |
| 24 | cg-fish |
| 25 | Speedy Algivore |
| 26 | cg-whale |
| 28 | RiverShark |
| 28 | Yellow Krill |
| 28 | 0-0-0 algae-seed |
| 29 | Armored Algivore |
| 30 | Salt Water Crocodiles |
| 30 | speedFish |
| 30 | FOERDI 1,1,0,1,A |
| 31 | ploplo |
| 31 | Shellder |
| 32 | gkdemaree-9-eater-of-drifting-forager |
| 33 | CookiePaladin |
| 35 | CookieMaximizer |
| 36 | VenomousRiverShark |
| 37 | Little fish |
| 37 | Coconutwhale |
| 37 | CookieAshimo |
| 38 | AlgaeProbably |
| 38 | Kun |
| 38 | gkdemaree-3-defensive-venum-forager |
| 38 | gkdemaree-4-tiny-slow-fish |
| 38 | Leuphonim |
| 38 | Chasnee |
| 39 | Phytoplankton |
| 39 | Algae Eater |
| 39 | Shrimp |
| 39 | Green Krill |
| 39 | Angy Shrimp |
| 39 | Meta-Algae |
| 39 | Greenglow Bindylow |
| 39 | Smallums |
| 39 | Zooplankton |
| 39 | Anchovy |
| 39 | Guppy |
| 39 | 0-0-0 algae |
| 39 | Algae Tribble |
| 40 | Plankton1 |
| 40 | Microblu |
| 40 | Orange Krill |
| 40 | Krill3 |
| 40 | Yonge_Ocean |
| 40 | CookieRunner |
| 40 | Blue Tang |
| 40 | FOERDI 0,0,0,0,A |
| 41 | Pike |
| 41 | Krill2 |
| 41 | Fornicacious Krill |
| 41 | Red Krill |
| 41 | Scumsipper |
| 41 | Herring |
| 41 | SpeedyFeedy |
| 41 | OceanEaty |
| 41 | Beaufish |
| 41 | Khisesant |
| 41 | Senuboon |
| 41 | Bhuphin |
| 42 | River Llama |
| 42 | gkdemaree-5-small-omnivore-fish |
| 42 | Snail-tooth Grabble |
| 42 | CookieWhite |
| 42 | CookieEater |
| 42 | CookieSzybcik |
| 42 | ocean omnivore |
| 42 | cg-fleshfish |
| 42 | Jilli |
| 42 | Sea snek run |
| 42 | FOERDI 0,3,0,3,A |
| 43 | RiverSharkFood |
| 43 | Sapphire Jelly |
| 43 | fooooley |
| 43 | defenseFish |
| 43 | Rosemary |
| 43 | Bissurus |
| 44 | Right whale |
| 44 | CookieShroom |
| 44 | Armor Fish |
| 44 | Groundlings |
| 45 | Slorp |
| 45 | gkdemaree-10-small-shark |
| 45 | Emerald Jelly |
| 45 | Patricians |
| 46 | Plage |
| 46 | gkdemaree-8-slow-armored-forager |
| 46 | Nano-krill |
| 47 | Electric eel |
| 47 | CookieDevourer |
| 47 | CookieMedoKinematic |
| 47 | Buneh Rizak-e Chenar |
| 48 | Megaladon |
| 48 | gkdemaree-6-large-omnivore-fish |
| 48 | Twinkle Star |
| 49 | algae_eater |
| 49 | Lilli |

# Rounds 51-100

By generation 51, only 8 species remain.

| Name | Venom + Antivenom? | Attack | Armor | Speed | Forager? | Creator |
|---|---|---|---|---|---|---|---|
| Killer whale | Neither | 9 | 1 | 10 | | Yair |
| gkdemaree-1-fast-shark | Antivenom only | 10 | 0 | 10 | | Grant Demaree |
| gkdemaree-2-fast-ocean-venum | Venom + Antivenom | 0 | 0 | 10 | Algae | Grant Demaree |
| gkdemaree-7-fast-forager-fish | Neither | 0 | 0 | 10 | Algae | Grant Demaree |
| Apex Predator | Antivenom only | 10 | 0 | 10 | | JSH |
| Killerspeed | Antivenom only | 10 | 0 | 7 | Carrion | VJ |
| Flesh-Eating Clam2 | Antivenom only | 0 | 10 | 0 | Carrion | Yull-Rete |
| Super-Armor Fish | Antivenom only | 0 | 10 | 0 | Carrion;Algae | Random guy in NY |

[![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-50-100.png)](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-50-100.png)

By Round 100, all that is left are the foragers with maxed-out defense. However, this time there are two niches: one Flesh-Eating Clam 2, which specializes in carrion, and another for Super-Armor Fish, which specializes in algae. 

# Rounds 101-9000

Alas, the Super-Armor Fish's ability to digest Algae lets it eventually outcompete the Flesh-Eating Clam2. A Soonbegon wanders in from time to time to eat the accumulated Detritus.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/ocean/ocean-100-9000.png)

# Winners

| Name | Venom + Antivenom? | Attack | Armor | Speed | Forager? | Creator |
|---|---|---|---|---|---|---|---|
| Super-Armor Fish | Antivenom only | 0 | 10 | 0 | Carrion;Algae | Random guy in NY |
| Soonbegon | Venom + Antivenom | 0 | 0 | 10 | Detritus | Randall|

