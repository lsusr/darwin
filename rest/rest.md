# Shore

The Shore is an inhospitable wasteland. Algae is available, but it's not very nitrutious. Coconuts offer an even worse calories-to-digestion ratio.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/palm-and-beach.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Shore | 0 | 0 | 0 | 0 | 20 | 1000 | 1000 | 10 |

No coconut eaters got established in the Human Garbage Dump, which means no coconut eaters got established in the River or Shore instead. The only available food is algae. The Shore is basically a small bit of Ocean biome with ⅒ of the algae. The algae-eating winner of our Ocean competition has zero speed so it cannot migrate to the Shore.

To top all of it off, predators migrate in from both sea and land.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/shore-0-9000.png)

Soonbegons migrate from time to time to eat the Detritus. Otherwise, the Shore is mostly a graveyard.

## Winners?

Soonbegons (kind of) by Martin Randall.

# Grassland

The grassland has lots of grass and even more seeds.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/grassland-by-yellowstone-lake-with-clouds-and-sky.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Grassland | 0 | 100 | 1000 | 2,000 | 0 | 0 | 0 | 50 |

The Grassland took 500 turns to establish an equilibrium. 

## Generations 1-500

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/grassland-0-500.png)

| Goes Extinct in Generation | Species |
|---|---|
| 11 | Meercat colony |
| 14 | Flock of birbs |
| 15 | Bool |
| 16 | Big Oof |
| 17 | Empowered Turtle |
| 17 | glpp511 |
| 18 | cg-bird |
| 19 | Nyarlathotep |
| 20 | Human |
| 21 | Tiny Snek |
| 21 | grass_mouse |
| 22 | Sleepypotat |
| 22 | Frontier-W8 |
| 23 | Armored Nutcracker |
| 23 | Toxikeet |
| 23 | lGha-S541 |
| 24 | Cowlagor |
| 27 | Karthosorox |
| 30 | Piranhakeet |
| 30 | cg-fastbird |
| 31 | Wampirek |
| 32 | Yonge_Snake |
| 34 | Sheep |
| 37 | Jackrabbit |
| 39 | Grassland Tribble |
| 39 | Grassland Aphid |
| 40 | basic seed fodder  |
| 40 | Galumphers |
| 40 | lGca-AS154 |
| 41 | GrassSeeater |
| 41 | Small Moth |
| 41 | SpeedyLichen |
| 41 | BeaupreyButGrassland |
| 41 | Tribble |
| 42 | DefinitelyJustARock |
| 44 | Weedle |
| 45 | Bob |
| 45 | SeedyEaty |
| 45 | Locust-Seeds |
| 47 | Tribble Hunter Hunter |
| 49 | Goat |
| 51 | seed fodder |
| 52 | Seed Eater Eater Eater |
| 53 | Frontier-W2 |
| 56 | Trash Panda1 |
| 56 | mediocre 1-2-s |
| 59 | Common Rat |
| 61 | Omnivorous Tribble Hunter |
| 62 | Frontier-W5 |
| 63 | Siolid |
| 65 | Cannibal Locust |
| 68 | Tribble Hunter Hunter Hunter Hunter |
| 73 | Ziarnojad Malutki |
| 74 | Horned Owl |
| 78 | Yonge_Omnivore |
| 83 | Medium Seedrat |
| 86 | Maverick Goose |
| 89 | Squish |
| 187 | Mooshroom |
| 294 | Killer Bunny |
| 345 | Untielopay |
| 423 | 6-0-3 seed |
| 466 | Yonge_Vegan |

## Generations 500-2000

The grassland establishes an equilibrium. The only native species left is the Hopsplitter.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/grassland-500-2000.png)

You can't see it but there's a single Venomoth on the bottom of the graph.

## Winners

| Species | Native Biome | Venom? | Weapons | Armor | Speed | Forage? | Temperature Adaptation? | Creator | Social Media |
|---|---|---|---|---|---|---|---|---|---|
| mediocre 1-3-s | Temperate Forest | Neither | 1 | 0 | 3 | Seeds |  | Corm | None |
| Hopsplitter | Grassland | Neither | 0 | 0 | 10 | Grass;Seeds |  | Nem | None |
| Brown Bear | Temperate Forest | Neither | 3 | 0 | 6 |  |  | elspood | None |
| Venomous Snark | Desert | Venom + Antivenom | 1 | 0 | 3 | Carrion | Heat (Allows survival in the Desert) | DaemonicSigil | [Twitter](https://twitter.com/lsindjowt) |
| Venomoth | Rainforest | Venom + Antivenom | 0 | 0 | 3 | Grass |  | aphyer | None |

# Temperate Forest

The temperate forest has lots of leaves and significant seeds.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/temperate-forest.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Temperate Forest | 0 | 2,000 | 100 | 1000 | 0 | 0 | 0 | 50 |

## Generations 1-200

The population crows, crashes, and then grows again.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/temperate-0-200.png)

| Goes Extinct in Generation | Species |
|---|---|
| 9 | Grizzly Bear |
| 12 | Optimistic Omnivore |
| 13 | LeavySpeedyTanky |
| 13 | The Dark Secret of Harvest Home |
| 14 | Colonoscopies |
| 15 | TFP511 |
| 15 | Gobbledygook |
| 16 | Boojum |
| 16 | Ankylosaurus |
| 16 | Monstrous Bandersnatch |
| 17 | Arboreal Assailant |
| 17 | Ultimate Lifeform  |
| 17 | Bear2 |
| 18 | BeauOmni2 |
| 21 | FastHerbivore |
| 22 | RockMonkey |
| 22 | Gino Soupprayen |
| 23 | Hedgie  |
| 24 | Basilisk |
| 24 | Meta-Lichen |
| 25 | Armored Pigeon |
| 26 | Lichen Bug |
| 26 | Fox |
| 26 | Predator |
| 29 | Forest Leaf Blight |
| 29 | Leaf Tribble |
| 31 | TreeSeeater |
| 31 | Yonge_Slug |
| 32 | Gipgip |
| 32 | You Worm! |
| 33 | Leafy Luncheoner |
| 33 | Caterpie |
| 33 | Spider |
| 34 | Forest Dragon |
| 34 | Woodland Locust |
| 34 | Forest Tribble2 |
| 40 | Munchers |
| 41 | Skitter Critter |
| 52 | SpeedySeedy |
| 57 | Unicorn |
| 57 | mediocre 1-3-s |
| 57 | lF-LM732 |
| 60 | Songalagala |
| 62 | Mango |
| 69 | Sky Shark |
| 70 | Locust Eater Eater |
| 71 | Brown Bear |
| 72 | Snake |
| 78 | Bullfrog |
| 79 | Pidgeotto |
| 81 | Fast Venomous Snake |
| 82 | Morpork Owl |
| 85 | Deadly Mickey K |
| 89 | lFa-G172 |
| 93 | Phanpy |
| 97 | Bear1 |
| 104 | Omnom |
| 109 | Ken Nishimura |
| 110 | Forest Scorpion |
| 114 | Yonge_Defense |
| 140 | Snark |
| 152 | Bastion |
| 169 | Titanosaur |

## Generations 200-2000

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/temperate-200-2000.png)

The system achieves stability stable (except for a random walk among equals).

| Goes Extinct in Generation | Species |
|---|---|
| 219 | Gypsy Moth |
| 272 | Armored Sloth |
| 290 | Donphan |
| 333 | Armadillo1 |
| 336 | Szaromysik |
| 342 | Turtling Trencherman |
| 762 | Bofa #0461-B |

## Winners

None of our final species have venom or antivenom.

| Species | Native Biome | Venom? | Weapons | Armor | Speed | Forage? | Creator | Social Media |
|---|---|---|---|---|---|---|---|---|
| Cutiefly | Temperate Forest | Neither | 0 | 0 | 0 | Seeds | alkjash | None |
| Brown Bear | Temperate Forest | Neither | 3 | 0 | 6 |  | elspood | None |
| Forest Finch | Rainforest | Neither | 0 | 0 | 3 | Leaves | MA | None |
| Snark | Temperate Forest | Neither | 0 | 0 | 10 | Leaves | Vanessa | None |
| Forest Tribble1 | Temperate Forest | Neither | 0 | 0 | 0 | Seeds | simon | None |
| Mutant Two-Headed Speed Snail | Human Garbage Dump | Neither | 0 | 0 | 2 | Grass | DaemonicSigil | [Twitter](https://twitter.com/lsindjowt) | None |

An honorable mention goes to the N054J's Forest Tribble2 which is identical to the Cutiefly and the Forest Tribble1.

# Rainforest

The Rainforest has lots of leaves and grass but few seeds.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/rainforest.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Rainforest | 0 | 1000 | 2,000 | 100 | 0 | 0 | 0 | 50 |]

## Generations 0-1000

It takes 1000 generations to establish an introduction.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/rainforest-0-1000.png)

| Goes Extinct in Generation | Species |
|---|---|
| 11 | Quartosaurus |
| 11 | Jon Rahoi |
| 12 | Stinging ant colony |
| 14 | Slug |
| 16 | Smok Wawelski Przezuwacz |
| 18 | Tree Clam |
| 19 | Slebbon |
| 22 | Rainmun |
| 25 | J's RLB |
| 25 | luden |
| 29 | Rainforest Leaf Blight |
| 31 | Tinysnek |
| 32 | Agyneta insolita |
| 36 | Crocodile |
| 37 | Giant Snake |
| 39 | Forest Ape |
| 40 | Very Lesser Forest Dragon |
| 42 | Birgus |
| 47 | Deerling |
| 49 | Elephant |
| 60 | lRa-G256 |
| 61 | Murder Hornet |
| 64 | Yonge_Rainforest |
| 66 | BeauOmni2ButRainforest |
| 79 | Jungle Hare |
| 81 | Stomporz |
| 84 | Raincow |
| 90 | Grazing Snake |
| 91 | Wombat |
| 93 | basic grass fodder |
| 95 | Armadillo2 |
| 98 | Rainforest Aphid |
| 100 | GrasseatyTank |
| 111 | Bofa 0461 |
| 119 | Arboreal Grazer |
| 127 | Boop |
| 128 | Nine-Banded Armadillo |
| 145 | Tell Masoud |
| 156 | Rain Tribble |
| 168 | Lily the Unicorn |
| 187 | Panther |
| 283 | Forest Finch |
| 305 | mediocre 1-3-g |
| 306 | Will Die |
| 316 | lRa-GLM281 |
| 318 | Fodder grass |
| 344 | Leaf Truck |
| 537 | Cheetah |

## Generations 1000-9000

The population oscillates from there. A small number of Snarks and Brown Bears are not visible.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/rest/rainforest-1000-9000.png)

| Goes Extinct in Generation | Species |
|---|---|
| 1485 | Rony |

## Winners

| Species | Native Biome | Venom? | Weapons | Armor | Speed | Forage? | Temperature Adaptation? | Creator |
|---|---|---|---|---|---|---|---|---|
| Venomoth | Rainforest | Venom + Antivenom | 0 | 0 | 3 | Grass |  | aphyer |
| LeavyTanky | Rainforest | Antivenom only | 0 | 10 | 0 | Leaves |  | ViktorThink |
| Untielopay | Grassland | Neither | 2 | 0 | 10 | Grass | Heat (Allows survival in the Desert) | Anonymous #5 |
| mediocre 1-3-s | Temperate Forest | Neither | 1 | 0 | 3 | Seeds |  | Corm |
| Forest Finch | Rainforest | Neither | 0 | 0 | 3 | Leaves |  | MA |
| Snark | Temperate Forest | Neither | 0 | 0 | 10 | Leaves |  | Vanessa |
