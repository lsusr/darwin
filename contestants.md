123 players submitted a total of 556 species. Most players submitted 1 species. One player submitted 11. (I disqualified his 11ᵗʰ submission.) 555 species qualified for entry. I expected more players would submit 10 species compared to 8 or 9 in order to max out their submissions but that didn't happen.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/entries-per-person.png)

The most popular name was "Krill", which three separate people submitted. This does not count "Yellow Krill", "Green Krill", "Red Krill", "Nano-krill" "Fornicacious Krill", "Orange-Krill" and "Orange Krill".

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/krill.png)

Other duplicate names included "Armadillo", "Bear", "Desert Tortoise", "Forest Tribble", "Kraken", "Trash Panda" and "Flesh-Eating Clam".

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/flesh-eating-clam.png)

A couple species were placed in the Tundra/Desert without the necessary temperature adaptations. I modified them so they wouldn't instantly die. Other people gave cold/heat adaptations to water-breathing species, which are useless. I left those unaltered.

Speaking of useless adaptations, Weapons + Armor is only useful up to 10. If your Weapons + Armor exceeds 10 the extra armor is useless. 40 submissions (7%) had Weapons + Armor more than 10. I think the idea here was to create big exciting powerful monsters. This is the realm of "Dragons", "Forest Dragons" "Basilisks", "Sandworms", "White-Whales", "Tundrus Rex", "Humans", "All-eating Leviathans" and the so-called "Ultimate Lifeform". I expect[^1] none of them will survive to the end.

[^1]: I'm writing these words before running the game.

I tried to render a food web but it didn't work because there were 54,867 connections. I let graphviz run for ten hours before putting it out of its misery.

# Stats

One sixth of the entries were venomous. One third had antivenom.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/venom-pie.png)

Unlike their number of entries, many players did choose to max out weapons, armor and speed.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/weapons.png)
![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/armor.png)
![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/speed.png)

# Spawning Locations

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/spawning-locations.png)

It makes sense that the ocean is popular because of how much food it supports. I was surprised by how popular the human garbage dump is. The Desert and Tundra were the least popular, presumably due to the temperature hazards.

# Foraging

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/forage-adaptations.png)

The most common foraging adaptations were algae, seeds and carrion.
- Algae is on top because there is the most algae.
- Seeds were popular because they offers lots of nutrition compared to the digestive adaptation.
- Carrion it is most nutritionally dense option.

# Forage or Hunt?

Half of all organisms were pure foragers. Of the rest, the majority had a foraging adaptation. Only a tiny minority were specialized hunters.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/hunt-or-forage.png)

# Social Media

I promised link to the winners' social media accounts (if they want). Most went with Twitter. Some went with LinkedIn, Tumblr, personal websites and Less Wrong itself. Two picked Instagram.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/entries/instagram-comic.png)
