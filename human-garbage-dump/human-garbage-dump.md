The human garbage dump is a place you can find anything. It was also (to me) surprisingly popular with 90 species submitted.

[TODO cover image]

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| Human Garbage Dump | 100 | 100 | 100 | 100 | 100 | 100 | 100 | 100 |

In each of our previous biomes, there was one primary source of forage. This is our first biome with many different viable options to forage. However, individually, none of them are very large. Even if there's no predators, the diverse primary production should ensure a little more biodiversity among foragers.

# Generations 0-1

We start with Anti Predator Flak. It's just like just like the Ocean's First Round Predator Distractor except it was created by Multicore.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-0-1.png)

| Goes Extinct in Generation | Species |
|---|---|
| 1 | Anti Predator Flak |

# Generations 1-10

Most of the initial populations crash, as usual.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-1-10.png)

The Seed Beetle is exactly what it sounds like: a tiny organism with the ability to digest seeds and nothing else.

| Goes Extinct in Generation | Species |
|---|---|
| 6 | Goo |
| 6 | Beauconut |
| 8 | Gelatinous Cube |
| 8 | Ivem |
| 10 | Chittering Harvester |
| 10 | Boot Nipper |
| 10 | Dump Leaf Blight |

# Generations 10-100

The Seed Beetle Population explodes. I think what's going on is something Multicore [pointed out](https://www.lesswrong.com/posts/AFXju94oCRKhhcG7k/the-2021-less-wrong-darwin-game?commentId=LL6rQcWgSEdSSSAKK) at the game launch. The Seed Beetle is so small that it provides inadequate nutrition to large carnivores. At first, predators eat the Seed Beetle, then the carnivores starve. The Seed Beetle is thus left without predators.

The Mutant Two-Headed Speed Snail is a Grass forager with Speed 2. The Beauprey forages for Seeds and Lichen and has Speed 1.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-10-100.png)

| Goes Extinct in Generation | Species |
|---|---|
| 11 | Trash Lobster |
| 11 | Carrion Condor |
| 11 | Roaches |
| 11 | Trash Panda2 |
| 11 | Muk1 |
| 12 | Small CocoCrab |
| 12 | The Fart Brigade |
| 12 | Thread-Nibbler |
| 12 | Dump Aphid |
| 12 | Slime Mold |
| 12 | mediocre 2-2-s2 |
| 12 | Muk2 |
| 13 | Olchi |
| 13 | TrashMonster3-2 |
| 14 | Sentient Bio Reactor |
| 14 | BottomFeeder |
| 14 | SpeedDemon |
| 14 | mediocre 2-2-g |
| 14 | TrashMonster2-2 |
| 15 | TrashMonster3-4 |
| 15 | TrashMonster4-3 |
| 16 | Big CocoCrab |
| 16 | Unpalatable Coconut Crab |
| 16 | TrashMonster4-2 |
| 17 | Rabbit |
| 17 | Lazy Stationary Glutton |
| 17 | sc511 |
| 17 | trashSeeds |
| 18 | Grease Monkey |
| 19 | Garbage Dump Shai-Hulud |
| 19 | Trolley |
| 20 | Coconut Crabs (tiny) |
| 20 | TrashMonster3-3 |
| 21 | Robot |
| 21 | Rat |
| 21 | CP445957 |
| 22 | mediocre 2-2-s1 |
| 23 | Frogger |
| 23 | Coconut Cruncher |
| 23 | Yonge_Dump |
| 23 | TrashMonster4-4 |
| 24 | All-eating Leviathan |
| 24 | Inquisition |
| 25 | Bleh |
| 25 | Robber Crab |
| 27 | Hyper fly |
| 28 | Coconut crab |
| 28 | Slow Moving Nuclear Waste |
| 29 | Garbage Disposal |
| 32 | Blitz Leech |
| 32 | 2-8-0 A algae-carrion-coconut-detritus-lichen |
| 32 | Jibbers Crabst |
| 33 | Snail-Eating Snail |
| 34 | Micro Swamp Dragon |
| 37 | Mecha giraffe |
| 37 | Twangoola |
| 38 | 25kg Racing Snake |
| 38 | Redneck |
| 40 | weaponized human |
| 44 | Garbovorous Mirages |
| 45 | Dirge Bat |
| 47 | AI Lawnmower |
| 48 | Trash Locust+ |
| 50 | gumbler |
| 50 | Cubic Carrion Crawlers |
| 56 | Trash Dino |
| 56 | CarrionSpeedTank |
| 75 | COCONUT511 |
| 89 | Lary |
| 89 | Gigantic Cockroach |
| 99 | Roomba |

# Generations 100-200

Species die out more slowly but there are no dramatic shifts in population.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-100-200.png)

| Goes Extinct in Generation | Species |
|---|---|
| 105 | hoover |
| 105 | Living Garbage Can |
| 107 | Ironshell Crab |
| 109 | Zigzagoon |
| 157 | Adventurous Giraffe |

# Generations 200-500

Nothing to see here.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-200-500.png)

| Goes Extinct in Generation | Species |
|---|---|
| 330 | Timblewinks |

# Generations 500-1000

Nothing to…wait what?

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-500-100-huh.png)

Let's take a closer look at what's going on.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-650-710.png)

There are nine species of consequence.

| Name | Venom? | Weapons | Armor | Speed | Forage? | Creator |
|---|---|---|---|---|---|---|
| Seed Beetle | Neither | 0 | 0 | 0 | Seeds | DaemonicSigil |
| Mutant Two-Headed Speed Snail | Neither | 0 | 0 | 2 | Grass | DaemonicSigil |
| Nuclear Waste | Antivenom only | 0 | 10 | 0 | Carrion | Henny |
| Brown Bear | Neither | 3 | 0 | 6 |  | elspood |
| Beauprey | Neither | 0 | 0 | 1 | Seeds;Lichen | Luke |
| Trash Slime | Antivenom only | 0 | 10 | 0 | Detritus | Yull-Rete |
| Forest Finch | Neither | 0 | 0 | 3 | Leaves | MA |
| Snark | Neither | 0 | 0 | 10 | Leaves | Vanessa |
| Hopsplitter | Neither | 0 | 0 | 10 | Grass;Seeds | Nem |

We have four invincible foragers: Nuclear Waste consumes Carrion. Trash Slime consumes Detritus. Snarks consume Leaves. Hopsplitters consume Grass and Seeds.

The Forest Finch competes for leaves with the Snark.

The Hopsplitters compete for seeds with the Seed Beetles and the Beaupreys. The Seed Beetle is a tiny organism optimized to consume seeds. The Beauprey is less efficeint at eating seeds than the Seed Beetle but makes up for it by also being able to digest Lichen.

The Mutant Two-Headed Speed Snail eats grass, where it competes with the Hopsplitter.

The Brown Bear is a pure predator. It cannot eat the invincible foragers but it can eat the Seed Beetles, Snails, Hopsplitters, Forest Finghes and Forest Finches.

Putting this all together, we have two kinds of foragers: small foragers that can be eaten by bears and invincible foragers those that cannot. If the bear population increases the small forager population decreases which increases the invincible forager population which decreases the bear population. This cycle works in reverse too. We have a stable ecosystem.

But if it's stable then what happened around generation 690?

The Seed Beetle population decreased because the Beauprey population increased. The Beauprey Population increased because there was lots of Lichen available. The Beauprey is the only species which can eat Lichen. Around generations 550 to 650 there were very few Beaupreys. The Detritus accumulated. The Beauprey's growth around population 680 ate that accumulated Detritus. After the Detritus was consumed, the Beauprey population decreased to something stable.

# Generations 1000-9000

The dynamic equilibrium is maintained until generation 8101 when the Beaupreys suddenly go extinct.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-1000-9000.png)

| Goes Extinct in Generation | Species |
|---|---|
| 8101 | Beauprey |

Let's zoom in.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-8000-8200.png)

What happened is the Brown Bear population randomly rose for long enough to extinguish the Beaupreys, which were already in a precarious position due to competition from the Seed Beetles..

But that's not all. I just counted the common species. There are a few rare species hiding on the bottom edge of our graph, like Snarks. The Snark is a 10-speed Leaf eater by Vanessa. The Beauprey, having Speed 1, comes back from time to time too.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/human-garbage-dump/hgd-8000-9000-minorities.png)

# Winners

| Species | Creator | Social Media |
|---|---|---|
| Seed Beetle | DaemonicSigil | [Twitter](https://twitter.com/lsindjowt) |
| Mutant Two-Headed Snail | DaemonicSigil | [Twitter](https://twitter.com/lsindjowt) |
| Nuclear Waste | Henny | None |
| Forest Finch | MA | None |
| Brown Bear | elspood | None |
| Trash Slim | Yull-Rete | None |
| Snark | Vanessa | None |
