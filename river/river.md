The River contains lots of every foragable food. The River is like the Human Garbage Dump except:
1. It has 5x as many resources.
2. Both air-breathing and water-breathing species can survive in the River.
3. Organisms may not start in the River. They must wander in.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/river/river-1471191056N1l.jpg)

| Name | Carrion | Leaves | Grass | Seeds | Detritus | Coconuts | Algae | Lichen |
|---|---|---|---|---|---|---|---|---|
| River | 500 | 500 | 500 | 500 | 500 | 500 | 500 | 500 |

# Generation 0

At first the foragables accumulate because there are no animals.

# Generations 1-100

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/river/river-0-100.png)

| Species | Original Biome | Venom? | Weapons | Armor | Speed | Forage? | Creator |
|---|---|---|---|---|---|---|---|
| Leafy Luncheoner | Temperate Forest | Neither | 0 | 0 | 1 | Leaves | Sean Hyer |
| Mooshroom | Grassland | Neither | 0 | 0 | 5 | Grass;Seeds | Vanessa |
| slow vulture | Desert | Antivenom only | 0 | 10 | 10 | Carrion | eat (Allows survival in the Desert),Corm |
| mediocre 1-2-s | Grassland | Neither | 1 | 0 | 2 | Seeds | Corm |
| Very Lesser Forest Dragon | Rainforest | Neither | 2 | 0 | 2 |  | Persephone 0461 |

The same pattern repeats for each foragable. First the foragable accomulates. Then a species which can consume the foragable appears. Its population explodes until it has eaten the accumulated foragable. Finally, its population reverts to carrying capacity.

Persephone 0461's Very Lesser Forest Dragon. It thrived until another predator appeared which hunted it to extinction.

# Generations 100-9000

Species appear. Species Disappear. Life is always in flux.

This is where all the Ocean's Soonbegons were coming from. Until they didn't anymore.

![](https://s3.us-west-2.amazonaws.com/www.lsusr.com/lesswrong/darwin-game-2/river/river-100-9000.png)

# Winners

| Name | Native Biome | Venom? | Attack | Armor | Speed | Forage? | Temperature Adaptation? | Creator | Social Media |
|---|---|---|---|---|---|---|---|---|---|---|
| Booyahs | Desert | Neither | 2 | 0 | 10 | Carrion | Heat (Allows survival in the Desert) | CK | None |
| Snark | Temperate Forest | Neither | 0 | 0 | 10 | Leaves |  | Vanessa | None |
| Venomoth | Rainforest | Venom + Antivenom | 0 | 0 | 3 | Grass |  | aphyer | None |
| Locust Eater Eater | Temperate Forest | Neither | 2 | 0 | 2 | Seeds |  | Multicore | None |
| Hopsplitter | Grassland | Neither | 0 | 0 | 10 | Grass;Seeds |  | Nem | None |
