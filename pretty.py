import pandas as pd
df = pd.read_csv('csvs/entries.csv')

import numpy as np
import matplotlib.pyplot as plt

  
N = 4
ind = np.arange(N) 
width = (1 / (N+1))
  
xvals = [8, 9, 2]
bar1 = plt.bar(ind, range(11), df['Weapons'].value_counts(), width, color = 'r')
  
#yvals = [10, 20, 30]
#bar2 = plt.bar(ind+width, range(11), width, color='g')
  
#zvals = [11, 12, 13]
#bar3 = plt.bar(ind+width*2, range(11), width, color = 'b')
  
plt.xlabel("Dates")
plt.ylabel('Scores')
plt.title("Players Score")
  
#plt.xticks(ind+width,['2021Feb01', '2021Feb02', '2021Feb03'])
#plt.legend( (bar1, bar2, bar3), ('Player1', 'Player2', 'Player3') )
plt.show()
